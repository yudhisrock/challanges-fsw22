let rock_p = document.getElementById ('r');
let paper_p = document.getElementById ('p');
let scissor_p = document.getElementById ('s');
let stat = document.getElementById ('ref');
let rock_c = document.getElementById ('rc');
let paper_c = document.getElementById ('pc');
let scissor_c = document.getElementById ('sc');
let refresh = document.getElementById ('refresh');
let x = 0;

function compchoice () {
    const choice = ['r', 'p', 's'];
    const random = (Math.floor(Math.random()*3));
    return choice [random];
}

function triggeruser (userchoice) {
    if (userchoice==="r") {
        rock_p.style.backgroundColor = "#C4C4C4";
        rock_p.style.borderRadius="30px";

        paper_p.removeAttribute('style');

        scissor_p.removeAttribute('style');
    }
    if (userchoice==="p") {
        paper_p.style.backgroundColor = "#C4C4C4";
        paper_p.style.borderRadius="30px";

        rock_p.removeAttribute('style');

        scissor_p.removeAttribute('style');
    }
    if (userchoice==="s") {
        scissor_p.style.backgroundColor = "#C4C4C4";
        scissor_p.style.borderRadius="30px";

        paper_p.removeAttribute('style');

        rock_p.removeAttribute('style');
    }
}

function nullified () {
       paper_p.removeAttribute('style');
       paper_c.removeAttribute('style');
       rock_p.removeAttribute('style');
       rock_c.removeAttribute('style');
       scissor_p.removeAttribute('style');
       scissor_c.removeAttribute('style');
    
       stat.removeAttribute('style');
       stat.innerHTML ="VS";
}

function win () {
    stat.innerHTML ="PLAYER 1 WIN";
    stat.style.width = "271.11px";
    stat.style.height ="166.93px";
    stat.style.fontSize ="38px";
    stat.style.backgroundColor ="#4C9654";
    stat.style.borderRadius = "10px";
    stat.style.paddingTop = "15%";
    stat.style.paddingLeft = "5%";
    stat.style.paddingRight = "5%";
    stat.style.color = "white";
    stat.style.transform = "rotate(-28.87deg)";
}

function lose () {
    stat.innerHTML ="COM WIN";
    stat.style.width = "271.11px";
    stat.style.height ="166.93px";
    stat.style.fontSize ="38px";
    stat.style.backgroundColor ="#4C9654";
    stat.style.paddingTop = "14%";
    stat.style.paddingLeft = "25%";
    stat.style.paddingRight = "25%";
    stat.style.borderRadius = "10px";
    stat.style.color = "white";
    stat.style.transform = "rotate(-28.87deg)";
}

function draw () {
    stat.innerHTML ="DRAW";
    stat.style.width = "271.11px";
    stat.style.height ="166.93px";
    stat.style.fontSize ="38px";
    stat.style.backgroundColor ="#035B0C";
    stat.style.paddingTop = "22%";
    stat.style.paddingLeft = "25%";
    stat.style.paddingRight = "25%";
    stat.style.borderRadius = "10px";
    stat.style.color = "white";
    stat.style.transform = "rotate(-28.87deg)";
}

function game (userchoice) {
    const comchoice = compchoice();
    if (comchoice==="r") {
        rock_c.style.backgroundColor = "#C4C4C4";
        rock_c.style.borderRadius="30px";
        rock_c.style.transition="all 0.5s ease";

        paper_c.removeAttribute('style');

        scissor_c.removeAttribute('style');
    }
    if (comchoice==="p") {
        paper_c.style.backgroundColor = "#C4C4C4";
        paper_c.style.borderRadius="30px";
        paper_c.style.transition="all 0.5s ease";

        rock_c.removeAttribute('style');

        scissor_c.removeAttribute('style');
    }
    if (comchoice==="s") {
        scissor_c.style.backgroundColor = "#C4C4C4";
        scissor_c.style.borderRadius="30px";
        scissor_c.style.transition="all 0.5s ease";

        paper_c.removeAttribute('style');

        rock_c.removeAttribute('style');
    }
    switch (userchoice + comchoice) {
        case "rs" :
        case "pr" :
        case "sp" :
            win ();
            break;
        case "rp" :
        case "ps" :
        case "sr" :
            lose ();
            break;
        case "rr" :
        case "pp" :
        case "ss" :
            draw();
             break;
    }
}

function main () {
    console.log(x);
    rock_p.addEventListener('click', function () {
        game("r");
        triggeruser("r");
     })
    
    paper_p.addEventListener('click', function () {
        game("p");
        triggeruser("p");
    })
    scissor_p.addEventListener('click', function () {
        game("s");
        triggeruser("s");
    }) 
    refresh.addEventListener('click', function () {
        nullified();
    })
    
}

main ();