const express = require('express');
const app = express();
const port = 3000;

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/views/gamefile'));
app.use(express.static(__dirname + '/views/webfile'))

app.get('/', function (req, res) {
  res.render('webfile/website-c3-fsw22.ejs');
})

app.get('/game', function (req, res) {
  res.render('gamefile/rps-c4-fsw-22.ejs');
})

app.listen(3000);